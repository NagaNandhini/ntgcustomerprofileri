using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace CustomerContactSync
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("sbt-ricontactsync-dev-sea-001", "customer-contact-masterdata-service-subscription", Connection = "MyServiceBus")] string mySbMsg, ILogger log)
        {
            log.LogInformation($"C# ServiceBus topic trigger function processed message: {mySbMsg}");
            log.LogTrace(mySbMsg);
        }
    }



    public class Rootobject
    {
        
    }

}
