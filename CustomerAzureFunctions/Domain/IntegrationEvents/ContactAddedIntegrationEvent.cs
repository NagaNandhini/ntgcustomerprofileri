﻿namespace CustomerProfile.Domain.IntegrationEvents
{
    using System;
    public class ContactAddedIntegrationEvent
    {
        public int Id { get; set; }
        public string correlationId { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string phoneExt { get; set; }
        public string cell { get; set; }
        public string email { get; set; }
        public int location { get; set; }
        public int customerId { get; set; }
        public DateTime dateOccurred { get; set; }

    }
}
