﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerProfile.Common.Events
{
    public class IntegrationEvent
    {
        protected IntegrationEvent()
        {
            DateOccurred = DateTimeOffset.UtcNow;
            CorrelationId = Guid.NewGuid();
        }

        public DateTimeOffset DateOccurred { get; protected set; } = DateTime.UtcNow;
       
        public Guid CorrelationId { get; set; }
    }
}
