﻿using CustomerAzureFunctions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AZFunction.UnitTests
{
    public class CustomerDataSyncUnitTest
    {
        [Fact]
        public async Task Verify_CustomerDataSync()
        {
            string inputMessage = "{\"correlationId\": \"f21d2d26-c053-4e68-a017-2e877e0a1c1e\",\"name\": \"Evan Raketic\",\"phone\": \"1234567812\",\"phoneExt\": \"1234\",\"cell\": \"1234562318\", \"email\": \"aa@aa.com\",\"location\": 9,\"customerId\": 172,\"dateOccurred\": \"2020-12-24T14:33:18.4254452+00:00\"}";
            var log = new LoggerFactory();
            await CustomerDataSync.Run(inputMessage, log.CreateLogger(""));

        }
    }
}
