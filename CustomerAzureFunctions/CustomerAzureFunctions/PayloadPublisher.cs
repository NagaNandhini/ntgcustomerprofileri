using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CustomerProfile.Domain.IntegrationEvents;

namespace CustomerAzureFunctions
{
    public static class PayloadPublisher
    {
        [FunctionName("PayloadPublisher")]
        [return: ServiceBus("sbt-ricontactsync-dev-sea-001", Connection = "sbtricontactsyncConStr")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            //string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var contactAddedIntergrationEvent = JsonConvert.DeserializeObject<ContactAddedIntegrationEvent>(requestBody);
            if (contactAddedIntergrationEvent == null)
                return new BadRequestObjectResult("Object is null");
            contactAddedIntergrationEvent.dateOccurred = DateTime.Now;

            log.LogInformation("JSON successfully deserialized" + contactAddedIntergrationEvent?.name);

            string responseMessage = JsonConvert.SerializeObject(contactAddedIntergrationEvent);
            log.LogInformation($"JSON successfully serialized after dateOccurred alerted {responseMessage}");

            return new OkObjectResult(responseMessage);
        }
    }
}
