﻿using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CustomerProfile.Domain.IntegrationEvents;
using System.Net.Http;
using System.Text;

namespace CustomerAzureFunctions
{
    public static class CustomerDataSync
    {
        [FunctionName("CustomerDataSyncFunc")]
        [return: ServiceBus("sbt-ricontactsync-ack-dev-sea-001", Connection = "sbtricontactsyncConStr")]
        public static async Task<string> Run([ServiceBusTrigger("sbt-ricontactsync-dev-sea-001", "customer-contact-masterdata-service-subscription",
            Connection = "sbtricontactsyncConStr")] string mySbMsg, ILogger log)
        {
            if (mySbMsg == "Success")
                return "200OK";
            var customerDataRequest = JsonConvert.DeserializeObject<CustomerDataRequest>(mySbMsg);

            var contactAddedIntergrationEvent = JsonConvert.DeserializeObject<ContactAddedIntegrationEvent>(customerDataRequest.Value);
            if (contactAddedIntergrationEvent == null)
                log.LogInformation("Error on incoming data");

            log.LogInformation($"correlationId { contactAddedIntergrationEvent?.correlationId}");
            log.LogInformation($"name { contactAddedIntergrationEvent?.name}");
            log.LogInformation($"cell { contactAddedIntergrationEvent?.cell}");
            log.LogInformation($"ServiceBus topic trigger function processed message: {customerDataRequest.Value}");
            string url = "Url";
            using (var client = new HttpClient())
            {
                if (url == "Url")
                {
                    log.LogInformation($"Static log message http response is OK");
                    return "Success";
                }
                var requestData = new StringContent(mySbMsg, Encoding.UTF8, "application/json");

                var response = await client.PostAsync(String.Format(url), requestData);
                var result = await response.Content.ReadAsStringAsync();
                log.LogInformation($" From Azure Function to target microservice {result}");
                return result;
            }


        }

    }

   


    public class CustomerDataRequest
    {
        public string Value { get; set; }
        public object[] Formatters { get; set; }
        public object[] ContentTypes { get; set; }
        public int StatusCode { get; set; }
    }


}
